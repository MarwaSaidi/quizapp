import React from "react";
import styled from "styled-components";

const ButtonComponent = styled.button`
  position: absolute;
  width: 18%;
  height: 60px;
  left: 75%;
  bottom: 0%;
  top: 760px;
  border: none;
  margin-bottom: 2%;
  background: #8692a6;
  border-radius: 30px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 19px;
  line-height: 28px;
  color: #ffffff;
  cursor: pointer;
  @media (max-width: 900px) {
    left: 200px;
  }
`;
const Button = ({ name, onClick }) => {
  return (
    <div>
      <ButtonComponent onClick={onClick}>{name}</ButtonComponent>
    </div>
  );
};

export default Button;
