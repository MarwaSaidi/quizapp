import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
const Timer = () => {
  const [timeLeft, setTimeLeft] = useState(600);
  const [isRunning] = useState(true);
  let navigate = useNavigate();
  useEffect(() => {
    let intervalId;
    if (isRunning) {
      intervalId = setInterval(() => {
        setTimeLeft(timeLeft - 1);
      }, 1000);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, timeLeft]);

  useEffect(() => {
    if (timeLeft === 0) {
      navigate("/");
    }
  }, [timeLeft, navigate]);

  //function formats the time
  function formatTimeLeft(time) {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds.toString().padStart(2, "0")}`;
  }

  return <> {formatTimeLeft(timeLeft)} </>;
};

export default Timer;
