export const categories = [
  {
    id: 1,
    img: "https://static.wixstatic.com/media/nsplsh_6a4b55324e6e655a416249~mv2.jpg/v1/crop/x_0,y_169,w_5472,h_3310/fill/w_763,h_460,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/Image%20by%20Clarisse%20Meyer.jpg",
    title: "History",
    cat: "History",
    model: "history",
  },
  {
    id: 2,
    img: "https://th.bing.com/th/id/OIP.KP55zCqpDHChbNhIhP8-4wAAAA?pid=ImgDet&rs=1",
    title: "Arts & Literature",
    cat: "Arts & Literature",
    model: "arts_and_literature",
  },
  {
    id: 3,
    img: "https://th.bing.com/th/id/OIP._9xixkZH8djEJr7jYkxIMQHaEK?w=282&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Film & TV",
    cat: "Film & TV",
    model: "film_and_tv",
  },
  {
    id: 4,
    img: "https://th.bing.com/th/id/OIP.kgYKJTfA3kHLHWysUPZX7AHaEJ?w=270&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Food & Drink",
    cat: "Food & Drink",
    model: "food_and_drink",
  },
  {
    id: 5,
    img: "https://th.bing.com/th/id/OIP.gpp63Xp3Xst_9UDdUCmvAQHaGW?w=208&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Geography",
    cat: "Geography",
    model: "geography",
  },
  {
    id: 6,
    img: "https://th.bing.com/th/id/OIP.OVLs-ty_WZ7Q_zBipcF5XAHaGU?w=188&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Science",
    cat: "Science",
    model: "science",
  },
  {
    id: 7,
    img: "https://th.bing.com/th/id/OIP.ZZvBDfQBo10B721M30QxEQHaDd?w=339&h=163&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Society & Culture",
    cat: "Society & Culture",
    model: "society_and_culture",
  },

  {
    id: 8,
    img: "https://th.bing.com/th/id/OIP.vP1hwPPqsiLLDxfYbPxaEAHaDj?w=290&h=167&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Sport & Leisure",
    cat: "Sport & Leisure",
    model: "sport_and_leisure",
  },
  {
    id: 9,
    img: "https://th.bing.com/th/id/OIP.mF8PLfhfKYf7BmCHhCs2aQHaDt?w=333&h=175&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "General Knowledge",
    cat: "General Knowledge",
    model: "general_knowledge",
  },
  {
    id: 10,
    img: "https://th.bing.com/th/id/OIP.Q2xeo-Xjikv-0AQFa30eaQHaFP?w=256&h=181&c=7&r=0&o=5&dpr=1.3&pid=1.7",
    title: "Music",
    cat: "Music",
    model: "music",
  },
];

export function getCategories() {
  return categories.filter((c) => c);
}

export function getCategory(model) {
  return categories.find((c) => c.model === model);
}

export const getQuestions = (cat) => {
  return fetch(
    `https://the-trivia-api.com/api/questions?categories=${cat}&limit=5`
  ).then((res) => res.json());
};
