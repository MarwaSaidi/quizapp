import React, { useEffect, useState } from "react";
import Button from "../commons/Button";
import { getCategory } from "../data";
import { Link, useLocation } from "react-router-dom";
import {
  Container,
  Title1,
  Title2,
  Div,
  ImgContainer,
  Image,
  InfoContainer,
  TextInfo,
  DescContainer,
  Title3,
  TextDesc,
} from "../styles/CategoryQuizDetails";

const CategoryQuizDetails = () => {
  const location = useLocation();
  const category = location.pathname.split("/")[2];
  const [categories, setCat] = useState({});
  const currentDate = new Date();
  const formattedDate = currentDate.toLocaleDateString("fr-FR", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  useEffect(() => {
    const getCat = async () => {
      try {
        const data = await getCategory(category);

        setCat(data);
      } catch {}
    };
    getCat();
  }, [category]);

  return (
    <Container>
      <Title1>{categories.title} Quiz</Title1>
      <Title2>Read the following instructions</Title2>
      <Div>
        <ImgContainer>
          <Image src={categories.img} />
        </ImgContainer>
        <InfoContainer>
          <Title3>
            Date: <TextInfo>{formattedDate}</TextInfo>
          </Title3>
          <Title3>
            Time Limit: <TextInfo>30min</TextInfo>{" "}
          </Title3>
          <Title3>
            Attempts: <TextInfo>Once</TextInfo>
          </Title3>
          <Title3>
            Points: <TextInfo>200 Points</TextInfo>
          </Title3>
        </InfoContainer>
        <DescContainer>
          <Title3>Instructions</Title3>
          <TextDesc>
            This quiz consists of 5 multiple-choice questions. To be successful
            with the quizzes, it's important to conversant with the topics. Keep
            the following in mind:
            <br />
            <br /> Timing - You need to complete each of your attempts in one
            sitting, as you are allotted 30 minutes to each attempt. Answers -
            You may review your answer-choices and compare them to the correct
            answers after your final attempt.
            <br />
            <br />
            To start,click the "Start" button. When finished, click the "Submit
            " button.
          </TextDesc>
        </DescContainer>
      </Div>
      <Link to={`/quizCategories/quiz/${category}`}>
        <Button name={"Start"} />
      </Link>
    </Container>
  );
};

export default CategoryQuizDetails;
