import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import {
  Container,
  SearchContainer,
  SearchInput,
  SearchIcon,
  Logo,
  ButtonStart,
  Profile,
  TextName,
  ImgProfile,
} from "../styles/NavBar";

const Navbar = () => {
  const name = localStorage.getItem("name");
  const parseName = JSON.parse(name);
  return (
    <div>
      <Container>
        <Logo>Quiz Time</Logo>

        <SearchContainer>
          <SearchInput placeholder="Search..." />
          <SearchIcon>
            <FontAwesomeIcon icon={faMagnifyingGlass} pull="left" />
          </SearchIcon>
        </SearchContainer>

        <Link to="/quizCategories">
          <ButtonStart>Start Quiz</ButtonStart>
        </Link>

        <Profile>
          <ImgProfile src="/assets/Ellipse.png" />
          <TextName>{parseName}</TextName>
        </Profile>
      </Container>
    </div>
  );
};

export default Navbar;
