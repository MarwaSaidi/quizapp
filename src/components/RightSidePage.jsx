import React from "react";
import {
  Container,
  TextContainer,
  Txt1,
  Txt2,
  Txt3,
  Vector,
} from "../styles/RightSidePage";

const RightSidePage = () => {
  return (
    <Container>
      <TextContainer>
        <Txt1>“</Txt1>
        <Txt2>
          Those people who develop the ability to continuously acquire new and
          better forms of knowledge that they can apply to their work and to
          their lives will be the movers and shakers in our society for the
          indefinite future
        </Txt2>
        <Txt3>Brian Tracy</Txt3>
        <Vector src="/assets/Vector1.png" />
      </TextContainer>
    </Container>
  );
};

export default RightSidePage;
