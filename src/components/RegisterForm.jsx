import React, { useState } from "react";
import styled from "styled-components";
import Input from "../commons/Input";
import ButtonComponent from "../commons/ButtonAuth";
import { register } from "../context/apiCalls";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Form = styled.form`
  position: absolute;
  width: 783px;
  height: 650px;
  left: 820px;
  top: 329px;
  @media (max-width: 900px) {
    left: 120px;
  }
`;

const RegisterForm = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleRegister = async (e) => {
    e.preventDefault();
    const errorMessage = await register({ email, name, password });
    if (errorMessage) {
      toast(errorMessage, {
        position: "top-center",
        type: "error",
        autoClose: 5000,
        closeOnClick: true,
        theme: "light",
      });
    } else {
      navigate("/login");
    }
  };
  return (
    <Form onSubmit={handleRegister}>
      <ToastContainer />
      <Input
        label={"Email address*"}
        placeholder={"Enter email address"}
        type={"email"}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />

      <Input
        label={"Your name*"}
        placeholder={"Enter your name"}
        type={"text"}
        value={name}
        onChange={(e) => setName(e.target.value)}
      />

      <Input
        label={"Password*"}
        placeholder={"Password"}
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />

      <br />
      <br />
      <br />
      <br />
      <br />
      <ButtonComponent title={"Register"} />
    </Form>
  );
};

export default RegisterForm;
