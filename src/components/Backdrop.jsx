import React, { useContext } from "react";
import Backdrop from "@mui/material/Backdrop";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalState";
import {
  Container,
  Logo,
  Text,
  Text1,
  ButtonContainer,
  Button,
} from "../styles/Backdrop";

const BackdropPass = ({ open }) => {
  const { score } = useContext(GlobalContext);
  return (
    <div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
      >
        <Container>
          <Logo src="/assets/Badge.png" />
          <Text>Congratulations you have passed</Text>
          <Text1>You scored {score} </Text1>
          <ButtonContainer>
            <Link to="/">
              {" "}
              <Button>New Quiz</Button>
            </Link>
          </ButtonContainer>
        </Container>
      </Backdrop>
    </div>
  );
};

export default BackdropPass;
