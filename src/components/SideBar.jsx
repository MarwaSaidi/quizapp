import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTable,
  faHeadset,
  faBell,
  faRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";
import { Link, useNavigate } from "react-router-dom";
import {
  Container,
  Button,
  Wrapper,
  TextItem,
  LogOutButton,
} from "../styles/SideBar";
import { logout } from "../context/apiCalls";
const SideBar = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    logout();
    navigate("/login");
  };
  return (
    <Container>
      <Link to="/">
        <Button>
          <FontAwesomeIcon icon={faTable} style={{ padding: " 0 10%" }} />
          Dashboard
        </Button>
      </Link>
      <Wrapper>
        <TextItem>
          <FontAwesomeIcon icon={faHeadset} style={{ padding: " 0 20%" }} />
          Support
        </TextItem>
        <TextItem>
          <FontAwesomeIcon icon={faBell} style={{ padding: " 0 23%" }} />
          Notification
        </TextItem>
        <LogOutButton onClick={handleLogout}>
          <FontAwesomeIcon
            icon={faRightFromBracket}
            style={{ padding: " 0 20%" }}
          />
          Log Out
        </LogOutButton>
      </Wrapper>
    </Container>
  );
};

export default SideBar;
