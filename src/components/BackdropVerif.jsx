import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Avatar from "@mui/material/Avatar";
import styled from "styled-components";

const Container = styled.div`
  position: absolute;
  width: 26%;
  height: 56%;
  min-width: 250px;
  min-height: 380px;
  left: 35%;
  top: 22%;
  background: #ffffff;
  border: 1px solid #eeeef0;
  backdrop-filter: blur(3px);
  border-radius: 30px;
`;
const Logo = styled.div`
  position: absolute;
  left: 36%;
  top: 20%;
  box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.25);
  border-radius: 56px;
`;
const Text = styled.p`
  position: absolute;
  width: 50%;
  left: 25%;
  right: 43.26%;
  top: 200px;
  bottom: 44.04%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 150%;
  text-align: center;
  letter-spacing: 0.5px;
  color: #000000;
`;

const ButtonContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 10px;
  position: relative;
  width: 70%;
  height: 10%;
  right: 43.26%;
  left: 13%;
  top: 80.27%;
  text-align: center;
  letter-spacing: 0.5px;
`;

const Button = styled.button`
  border: none;
  background: #ffffff;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 28px;
  cursor: pointer;
`;

const BackdropVerif = ({ onClick, open, onVerif }) => {
  return (
    <div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
      >
        <Container>
          <Logo>
            <Avatar
              sx={{ bgcolor: "#4B5548", width: 88, height: 88, fontSize: 40 }}
            >
              ?
            </Avatar>
          </Logo>
          <Text>Are you Sure you want to submit Quiz?</Text>
          <ButtonContainer>
            <Button onClick={onClick}>No</Button>
            <Button onClick={onVerif}>Yes</Button>
          </ButtonContainer>
        </Container>
      </Backdrop>
    </div>
  );
};

export default BackdropVerif;
