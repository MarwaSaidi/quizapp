import React, { useState } from "react";
import Input from "../commons/Input";
import CheckboxComponent from "../commons/Checkbox";
import ButtonComponent from "../commons/ButtonAuth";
import { Form, ButtonGoogle, HrOr, Logo } from "../styles/LoginForm";
import { login } from "../context/apiCalls";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    const errorMessage = await login({ email, password });
    if (errorMessage) {
      toast(errorMessage, {
        position: "top-center",
        autoClose: 5000,
        type: "error",
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: 0,
        theme: "light",
      });
    } else {
      navigate("/");
    }
  };
  return (
    <Form onSubmit={handleLogin}>
      <ToastContainer />
      <Input
        label={"Email address*"}
        placeholder={"Enter email address"}
        type={"email"}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />

      <Input
        label={"Enter password*"}
        placeholder={"Password"}
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <CheckboxComponent />
      <ButtonComponent title={"Login"} />
      <HrOr />
      <ButtonGoogle>
        <Logo src="https://img.icons8.com/color/512/google-logo.png"></Logo>
        Login with Google
      </ButtonGoogle>
    </Form>
  );
};

export default LoginForm;
