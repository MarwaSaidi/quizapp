import React, { useState, useEffect, useContext } from "react";
import { GlobalContext } from "../context/GlobalState";
import Button from "../commons/Button";
import { useLocation } from "react-router-dom";
import BackdropVerif from "./BackdropVerif";
import BackdropPass from "./Backdrop";
import Timer from "../commons/Timer";
import { getQuestions, getCategory } from "../data";
import {
  Container,
  Title1,
  Title1_2,
  Title2,
  Div,
  ImgContainer,
  Image,
  QuestionContainer,
  Title3,
  TextQuestion,
  AnswerContainer,
  Ul,
  Li,
  TextAnswer,
  Radio,
  True,
} from "../styles/Quiz";

const Quiz = () => {
  const location = useLocation();
  const category = location.pathname.split("/")[3];
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [selectedValue, setSelectedValue] = useState("");
  const [verifFalse, setVerifFalse] = useState("");
  const [verifTrue, setVerifTrue] = useState("");
  const [open, setOpen] = useState(false);
  const [openBD, setopenBD] = useState(false);
  const { handleAnswerOption } = useContext(GlobalContext);
  const data = getCategory(category);
  //close  BackdropVerif and BackdropPass
  const handleClose = () => {
    setOpen(false);
    setopenBD(false);
  };
  //open  BackdropVerif and BackdropPass
  const handleToggle = () => {
    setOpen(true);
    setopenBD(true);
  };
  //verification correct response and false reasponse
  const handleVerif = () => {
    setOpen(false);
    if (selectedValue === questions[currentQuestion].correctAnswer) {
      setVerifTrue("correct Answer");
      handleAnswerOption(true);
    } else {
      setVerifTrue("correct Answer");
      setVerifFalse("incorrect Answers");
    }
  };
  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };
  //pass next question
  const handelNext = () => {
    const nextQuestion = currentQuestion + 1;
    setCurrentQuestion(nextQuestion);
    setVerifTrue("");
    setVerifFalse("");
  };

  useEffect(() => {
    getQuestions(category).then((questions) => setQuestions(questions));
  }, [category]);

  // const Answers=()=>{
  //   try {
  //     const incorrectAnswers=questions[currentQuestion].incorrectAnswers;
  //     const correctAnswers=questions[currentQuestion].correctAnswer;
  //     const answers=[correctAnswers,...incorrectAnswers];
  //     if (Array.isArray(answers)) {
  //     return answers.sort(() => Math.random() - 0.5);
  //     } else {
  //       throw new Error('The value returned by Answers() is not an array');
  //     }
  //   } catch (e) {
  //     return "wait ... ";
  //   }
  // }
  return (
    <Container>
      {questions.length > 0 ? (
        <>
          <Title1>{data.title} Quiz </Title1>
          <Title1_2>
            Timer:
            <Timer />
            Mins
          </Title1_2>
          <Title2>Answer the question below</Title2>

          <Div>
            <ImgContainer>
              <Image src="https://img.freepik.com/vecteurs-premium/ensemble-points-interrogation-dessines-main-illustration-vectorielle_36380-850.jpg" />
            </ImgContainer>
            <QuestionContainer>
              <Title3>
                Question {currentQuestion + 1}/{questions.length}
              </Title3>
              <TextQuestion>{questions[currentQuestion].question}</TextQuestion>
            </QuestionContainer>
            <AnswerContainer>
              <Title3>Choose answer</Title3>
              <Ul>
                {questions[currentQuestion].incorrectAnswers.map((ans, i) => {
                  return (
                    <Li key={i}>
                      <Radio
                        type="radio"
                        value={ans}
                        checked={selectedValue === ans}
                        afterContent={verifFalse}
                        disabled={verifFalse !== "" && verifTrue !== ""}
                        onChange={handleChange}
                      />
                      <TextAnswer>{ans}</TextAnswer>
                    </Li>
                  );
                })}

                <Li>
                  <Radio
                    type="radio"
                    value={questions[currentQuestion].correctAnswer}
                    disabled={verifTrue !== ""}
                    checked={
                      selectedValue === questions[currentQuestion].correctAnswer
                    }
                    onChange={handleChange}
                  />
                  <TextAnswer>
                    {questions[currentQuestion].correctAnswer}
                  </TextAnswer>
                  <True>{verifTrue}</True>
                </Li>
              </Ul>
            </AnswerContainer>
          </Div>

          {currentQuestion + 1 === questions.length && verifTrue !== "" ? (
            <>
              <BackdropPass open={openBD} />
            </>
          ) : (
            <>
              <Button
                name={"Next"}
                onClick={verifTrue ? handelNext : handleToggle}
              />
            </>
          )}
        </>
      ) : (
        <div>Loading...</div>
      )}
      <BackdropVerif onVerif={handleVerif} onClick={handleClose} open={open} />
    </Container>
  );
};

export default Quiz;
