import React from "react";
import Navbar from "../components/NavBar";
import SideBar from "../components/SideBar";
import { Navigate, Outlet } from "react-router-dom";

const HomeLayout = () => {
  const token = localStorage.getItem("token");
  if (!token) return <Navigate to="/login" />;
  return (
    <div>
      <Navbar />
      <SideBar />
      <Outlet />
    </div>
  );
};

export default HomeLayout;
