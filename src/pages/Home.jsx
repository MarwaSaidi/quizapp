import React, { useContext } from "react";
import CategoryItem from "../components/CategoryItem";
import { categories } from "../data";
import { Link } from "react-router-dom";
import Slider from "@mui/material/Slider";
import Box from "@mui/material/Box";
import { GlobalContext } from "../context/GlobalState";
import {
  Container,
  ImageContainer,
  Image,
  Name,
  LogoDiv,
  LogoContainer,
  LogoText1,
  LogoText2,
  TitleContainer,
  Title,
  ViewALL,
  CategoriesContainer,
  AchvementContainer,
  ImgBadge,
  ImgBadge2,
  ImgBadge3,
  TitleBadge,
  TitleBadge2,
  TitleBadge3,
  Div,
  P,
} from "../styles/Home";

const Home = () => {
  const { corectAns, score } = useContext(GlobalContext);
  const name = localStorage.getItem("name");
  const parseName = JSON.parse(name);
  return (
    <Container>
      <ImageContainer>
        <Image src="/assets/profile.png" />
      </ImageContainer>
      <Div>
        <Name>{parseName}</Name>
        <P>Bonus booster 24lv</P>
        <Box sx={{ width: 650, height: 30 }}>
          <Slider disabled defaultValue={70} aria-label="Disabled slider" />
        </Box>
        <LogoDiv>
          <LogoContainer>
            <img src="/assets/flag.png" />
          </LogoContainer>
          <LogoText2>
            <LogoText1>{score}</LogoText1> Quiz Passed
          </LogoText2>
          <LogoContainer>
            <img src="/assets/time.png" />
          </LogoContainer>
          <LogoText2>
            <LogoText1>27min</LogoText1> Fastest Time
          </LogoText2>
          <LogoContainer>
            <img src="/assets/logo.png" />
          </LogoContainer>
          <LogoText2>
            <LogoText1>{corectAns}</LogoText1> Correct Answers
          </LogoText2>
        </LogoDiv>
      </Div>
      <TitleContainer>
        <Title>Achievements </Title>
        <Title>Featured Category</Title>
        <Link to="/quizCategories">
          <ViewALL>View All</ViewALL>
        </Link>
      </TitleContainer>
      <AchvementContainer>
        <ImgBadge src="/assets/Badge (1).png" />
        <TitleBadge>Comeback</TitleBadge>
        <ImgBadge2 src="/assets/Badge (3).png" />
        <TitleBadge2>Winner</TitleBadge2>
        <ImgBadge3 src="/assets/Badge (2).png" />
        <TitleBadge3>Lucky</TitleBadge3>
      </AchvementContainer>
      <CategoriesContainer>
        {categories
          .map((item) => <CategoryItem item={item} key={item.id} />)
          .slice(0, 4)}
      </CategoriesContainer>
    </Container>
  );
};

export default Home;
