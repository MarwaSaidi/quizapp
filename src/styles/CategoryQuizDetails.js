import styled from "styled-components";
export const Container = styled.div`
  position: absolute;
  width: 75%;
  height: 120%;
  left: 21%;
  top: 120px;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 30px;
  overflow: auto;
  @media (max-width: 900px) {
    left: 190px;
  }
`;
export const Title1 = styled.p`
  position: absolute;
  left: 45px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 33px;
  line-height: 50px;
  color: #696f79;
`;

export const Title2 = styled.p`
  position: relative;
  width: 900px;
  height: 30px;
  margin: 80px 45px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 20px;
  line-height: 30px;
  color: #696f79;
`;
export const Div = styled.div`
  position: absolute;
  width: 90%;
  height: 80%;
  left: 3.5%;
  top: 120px;
`;
export const ImgContainer = styled.div`
  position: absolute;
  width: 48.5%;
  height: 48%;
  top: 1px;
  flex: 1;

  border-radius: 30px;
  box-shadow: 0px 15px 40px 5px #ededed;
  @media (max-width: 900px) {
    height: 280px;
    top: 30px;
  }
`;
export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 30px;
`;
export const InfoContainer = styled.p`
  position: absolute;
  width: 45%;
  height: 45%;
  top: -5px;
  margin-left: 52%;
  align-items: center;
  @media (max-width: 900px) {
    top: 20px;
  }
`;
export const TextInfo = styled.p`
  margin-left: 170px;
  margin-top: -30px;
  padding: 0%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 20px;
  line-height: 30px;
  color: #696f79;
`;

export const Title3 = styled.h3`
  height: 20%;
  margin-top: 0px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 23px;
  line-height: 34px;
  color: #696f79;
`;
export const DescContainer = styled.div`
  position: absolute;
  width: 105%;
  height: 290px;
  top: 330px;
  display: 1;
  align-items: center;
  margin: 3% 0%;
  overflow: auto;
  @media (max-width: 900px) {
    width: 520px;
  }
`;
export const TextDesc = styled.p`
  margin-top: -2%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 110%;
  line-height: -20%;
  display: flex;
  align-items: center;
  color: #696f79;
`;
