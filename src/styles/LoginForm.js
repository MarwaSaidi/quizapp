import styled from "styled-components";

export const Form = styled.form`
  position: absolute;
  width: 783px;
  height: 650px;
  left: 820px;
  top: 329px;

  @media (max-width: 900px) {
    left: 120px;
  }
`;

export const ButtonGoogle = styled.button`
  position: absolute;
  width: 426px;
  height: 64px;
  top: 500px;

  border: none;
  background: #ffffff;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 30px;

  font-family: "Poppins";
  font-style: normal;
  font-weight: 500;
  font-size: 19px;
  line-height: 23px;
  align-items: center;
  text-align: center;
`;

export const HrOr = styled.hr`
  position: absolute;
  width: 350px;
  height: 0px;
  left: 15px;
  top: 500px;
  margin: -25px 0px;
  border: 0.5px solid #f5f5f5;
`;
export const Logo = styled.img`
  position: absolute;
  width: 24px;
  height: 24px;
  left: 43px;
  top: 21px;
`;
