import styled from "styled-components";
export const Container = styled.div`
  height: 120px;
  width: 98%;
`;
export const SearchContainer = styled.div`
  position: absolute;
  width: 23%;
  height: 65px;
  left: 22.5%;
  top: 5%;
  box-shadow: 0px 15px 40px 5px #ededed;
  border: none;
  border-radius: 30px;
`;

export const SearchInput = styled.input`
  position: absolute;
  width: 60%;
  height: 55%;
  left: 20%;
  top: 25%;
  border: none;
  outline: none;
  ::placeholder {
    font-family: "Poppins";
    font-style: normal;
    font-weight: 400;
    font-size: 19px;
    line-height: 28px;
    color: #696f79;
  }
`;
export const SearchIcon = styled.div`
  position: absolute;
  width: 75px;
  height: 29px;
  left: 10%;
  top: 35%;
`;

export const Logo = styled.h1`
  position: absolute;
  width: 15%;
  height: 20%;
  top: 3%;
  margin-left: 6%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 800;
  font-size: 30px;
  line-height: 45px;
  color: #696f79;
`;

export const ButtonStart = styled.button`
  position: absolute;
  width: 15%;
  height: 65px;
  left: 56.5%;
  top: 5.5%;
  border: none;
  background: #8692a6;
  border-radius: 30px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 500;
  font-size: 19px;
  align-items: center;
  text-align: center;
  cursor: pointer;
  color: #ffffff;
  @media (max-width: 900px) {
    width: 150px;
    height: 45px;
  }
`;
export const Profile = styled.div`
  position: absolute;
  width: 10%;
  height: 10%;
  left: 78%;
  top: 5%;
  flex: 1;
  display: flex;
  justify-content: flex-end;

  @media (max-width: 900px) {
    padding-left: 100px;
  }
`;
export const TextName = styled.p`
  font-family: "Poppins";
  font-size: 19px;
  line-height: 28px;
  display: flex;
  color: #696f79;
`;
export const ImgProfile = styled.img`
  width: 50%;
  height: 100%;
  margin-right: 8%;
`;
