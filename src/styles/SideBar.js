import styled from "styled-components";
export const Container = styled.div`
  position: absolute;
  width: 20%;
  height: 100%;
  @media (max-width: 900px) {
    height: 680px;
  }
`;
export const Button = styled.button`
  position: absolute;
  width: 90%;
  height: 8.5%;
  left: 7%;
  top: 2%;
  border: none;
  background: #8692a6;
  border-radius: 30px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 19px;
  line-height: 28px;
  color: #ffffff;
  cursor: pointer;
  @media (max-width: 900px) {
    width: 150px;
    height: 55px;
  }
`;
export const Wrapper = styled.div`
  position: relative;
  width: 90%;
  top: 15%;
  left: -2%;
  display: grid;
  place-items: center;
`;
export const TextItem = styled.div`
  padding-right: 10%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 120%;
  margin-bottom: 20%;
  display: flex;
  align-items: center;
  color: #696f79;
`;
export const LogOutButton = styled.button`
  position: absolute;
  font-family: "Poppins";
  font-style: normal;
  background: white;
  border: none;
  font-weight: 600;
  font-size: 20px;
  line-height: 30px;
  top: 350%;
  display: flex;
  align-items: center;
  color: #696f79;
  cursor: pointer;
  @media (max-width: 900px) {
    top: 320px;
  }
`;
