import styled from "styled-components";
export const Container = styled.div`
  position: absolute;
  width: 75%;
  height: 120%;
  left: 21%;
  top: 120px;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 30px;
  overflow: auto;
  @media (max-width: 900px) {
    left: 190px;
  }
`;
export const Title1 = styled.p`
  position: absolute;
  width: 500px;
  height: 50px;
  left: 45px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 33px;
  line-height: 50px;
  color: #696f79;
`;
export const Title1_2 = styled.p`
  position: absolute;
  width: 24%;
  height: 50px;
  min-width: 270px;
  left: 75%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 200%;
  line-height: 50px;
  color: #696f79;
`;
export const Title2 = styled.p`
  position: relative;
  width: 900px;
  height: 30px;
  margin: 80px 45px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 20px;
  line-height: 30px;
  color: #696f79;
`;
export const Div = styled.div`
  position: absolute;
  width: 92%;
  height: 82%;
  left: 40px;
  top: 133px;
`;

export const ImgContainer = styled.div`
  width: 48.5%;
  height: 42%;
  flex: 1;
  border-radius: 30px;
  box-shadow: 0px 15px 40px 5px #ededed;
  @media (max-width: 900px) {
    top: 120px;
    height: 190px;
  }
`;
export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 30px;
`;
export const QuestionContainer = styled.p`
  position: absolute;
  width: 48%;
  height: 40.5%;
  top: -3%;
  left: 52.5%;
`;
export const Title3 = styled.p`
  height: 50px;
  margin-top: 0px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 23px;
  line-height: 34px;
  color: #696f79;
`;
export const TextQuestion = styled.p`
  margin: -10px 0px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 27px;
  color: #696f79;
`;
export const AnswerContainer = styled.div`
  width: 99%;
  height: 55%;
  display: 1;
  justify-content: space-between;
  align-items: center;
  margin: 30px 0px;
`;
export const Ul = styled.ul`
  display: 1;
  justify-content: space-between;
  margin: -20px -50px;
  list-style-type: none;
  padding-bottom: 10px;
`;
export const Li = styled.li`
  padding-bottom: 25px;
`;
//export const Radio = styled.input``;
export const TextAnswer = styled.label`
  padding: 20px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 27px;
  color: #696f79;
`;

export const Radio = styled.input`
  &:checked:after {
    content: "${(props) => props.afterContent}";
    display: inline-block;
    margin-left: 3600%;
    width: 181px;
    font-weight: 700;
    font-family: "Poppins";
    font-style: normal;
    font-size: 18px;
    line-height: 27px;
    margin-top: -25px;
    color: red;
    @media (max-width: 900px) {
      margin-left: 230px;
    }
  }
`;
export const True = styled.p`
  display: inline-block;
  margin-left: 23.5%;
  text-align: center;
  font-weight: 700;
  font-family: "Poppins";
  font-style: normal;
  font-size: 18px;
  line-height: 27px;
  margin-top: -25px;
  color: green;
  @media (max-width: 900px) {
    margin-left: 10px;
  }
`;
