import styled from "styled-components";

export const Container = styled.div`
  position: absolute;
  width: 75%;
  height: 120%;
  left: 21%;
  top: 120px;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 30px;
  overflow: auto;
  @media (max-width: 900px) {
    left: 190px;
  }
`;
export const ImageContainer = styled.div`
  position: absolute;
  width: 26%;
  height: 30%;
  left: 3%;
  top: 7%;
  align-items: center;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 30px;
  @media (max-width: 1150px) {
    left: 30px;
    width: 140px;
    height: 150px;
  }
`;
export const Image = styled.img`
  position: absolute;
  width: 120%;
  height: 130%;
  left: -10%;
  top: -10%;
  object-fit: cover;
`;

export const Div = styled.div`
  position: absolute;
  width: 64%;
  height: 35%;
  top: 6%;
  left: 32.5%;
  align-items: center;
  @media (max-width: 900px) {
    left: 190px;
  }
`;

export const Name = styled.div`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 33px;
  line-height: 50px;
  display: flex;
  align-items: center;
  color: #696f79;
`;
export const LogoDiv = styled.div`
  position: absolute;
  width: 90%;
  height: 50%;
  display: flex;
  align-items: center;
  @media (max-width: 900px) {
    top: 110px;
  }
`;
export const LogoContainer = styled.div`
  padding: 3%;
  margin-top: 50px;
  width: 90px;
  height: 35px;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 15px;
`;
export const LogoText1 = styled.div`
  margin-right: -10%;
  padding: -2%;
  width: 160px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 29px;
  line-height: 44px;
  color: #696f79;
`;
export const LogoText2 = styled.div`
  width: 24%;
  padding: 10px;
  margin-top: 30px;
  font-family: "Poppins";
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  color: #696f79;
`;
export const TitleContainer = styled.div`
  position: absolute;
  width: 98%;
  height: 7%;
  left: 3%;
  top: 380px;
  display: flex;
  align-items: inline;
  @media (max-width: 900px) {
    width: 700px;
  }
`;
export const Title = styled.p`
  margin: 1% 30%;
  margin-left: 0.5%;
  font-family: "Poppins";
  font-weight: 700;
  font-size: 25px;
  line-height: 38px;
  color: #696f79;
  @media (max-width: 900px) {
    margin: 5px 100px;
    margin-left: 10px;
  }
`;
export const ViewALL = styled.button`
  position: absolute;
  width: 80px;
  left: 87%;
  top: 25%;
  font-family: "Poppins";
  border: none;
  background: #ffffff;
  font-style: normal;
  font-weight: 400;
  font-size: 17px;
  line-height: 24px;
  text-align: center;
  color: #696f79;
  cursor: pointer;
  @media (max-width: 900px) {
    left: 600px;
  }
`;
export const CategoriesContainer = styled.div`
  position: absolute;
  width: 48%;
  height: 45%;
  left: 48%;
  top: 450px;
  display: grid;

  grid-template-columns: repeat(2, 1fr);
  @media (max-width: 900px) {
    left: 280px;
  }
`;
export const AchvementContainer = styled.div`
  position: absolute;
  width: 43%;
  height: 400px;
  left: 3%;
  top: 450px;
  background: #ffffff;
  box-shadow: 0px 15px 40px 5px #ededed;
  border-radius: 30px;
  @media (max-width: 900px) {
    width: 280px;
  }
`;
export const ImgBadge = styled.img`
  position: absolute;
  left: 8%;
  top: 16.5%;
`;
export const ImgBadge2 = styled.img`
  position: absolute;
  left: 66%;
  top: 16.5%;
`;
export const ImgBadge3 = styled.img`
  position: absolute;
  left: 36%;
  top: 60%;
`;
export const TitleBadge = styled.p`
  position: absolute;
  left: 10%;
  top: 44%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;
  color: #696f79;
  align-items: ;
`;
export const TitleBadge2 = styled.p`
  position: absolute;
  left: 70%;
  top: 44%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;
  color: #696f79;
`;
export const TitleBadge3 = styled.p`
  position: absolute;
  left: 42%;
  top: 85%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;
  color: #696f79;
`;

export const P = styled.p`
  margin-top: -10px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 19px;
  line-height: 28px;
  display: flex;
  align-items: center;
  color: #696f79;
`;
