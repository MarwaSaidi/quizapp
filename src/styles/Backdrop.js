import styled from "styled-components";

export const Container = styled.div`
  box-sizing: border-box;
  position: absolute;
  width: 33.5%;
  height: 55%;
  min-width: 430px;
  min-height: 380;
  left: 32%;
  top: 20%;
  background: #ffffff;
  border: 1px solid #eeeef0;
  backdrop-filter: blur(3px);
  border-radius: 30px;
`;
export const Logo = styled.img`
  position: absolute;
  left: 42%;
  top: 15%;
`;
export const Text = styled.p`
  position: absolute;
  width: 80%;
  left: 10%;
  right: 43.26%;
  top: 45.27%;
  bottom: 44.04%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 19px;
  line-height: 150%;
  text-align: center;
  letter-spacing: 0.5px;
  color: #000000;
`;
export const Text1 = styled.p`
  position: absolute;
  width: 80%;
  left: 10%;
  right: 43.26%;
  top: 55.27%;
  text-align: center;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 150%;
  color: #000000;
`;
export const ButtonContainer = styled.div`
  position: absolute;
  width: 100%;

  right: 43.26%;
  left: 7.5%;
  top: 80.27%;
  text-align: center;
  letter-spacing: 0.5px;
`;
export const Button = styled.button`
  border: none;
  background: #ffffff;
  margin-right: 90px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 28px;
  cursor: pointer;
`;
