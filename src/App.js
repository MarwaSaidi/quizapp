import React from "react";
import Login from "./pages/Login";
import Register from "./pages/Register";
import NotFound from "./pages/NotFound";
import Categories from "./components/Categories";
import CategoryQuizDetails from "./components/CategoryQuizDetails";
import HomeLayout from "./pages/HomeLayout";
import Quiz from "./components/Quiz";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import { GlobalProvider } from "./context/GlobalState";
function App() {
  return (
    <GlobalProvider>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/Register" element={<Register />} />

        <Route path="/" element={<HomeLayout />}>
          <Route path="" element={<Home />} />
          <Route path="quizCategories" element={<Categories />} />
          <Route path="quizCategories/:cat" element={<CategoryQuizDetails />} />
          <Route path="quizCategories/quiz/:cat" element={<Quiz />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </GlobalProvider>
  );
}

export default App;
