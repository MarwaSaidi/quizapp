import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";
//initial state
const initialState = {
  score: 0,
  corectAns: 0,
};

//create context
export const GlobalContext = createContext(initialState);
//Provider Component
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);
  //Actions
  function handleAnswerOption(correctAnswer) {
    dispatch({
      type: "HANDEL_ANSWER_OPTION",
      payload: correctAnswer === true,
    });
  }

  return (
    <GlobalContext.Provider
      value={{
        score: state.score,
        corectAns: state.corectAns,
        handleAnswerOption,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
