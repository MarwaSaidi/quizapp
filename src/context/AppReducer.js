export default (state, action) => {
  switch (action.type) {
    case "HANDEL_ANSWER_OPTION":
      return {
        score: action.payload ? state.score + 20 : state.score,
        corectAns: action.payload ? state.corectAns + 1 : state.corectAns,
      };
    default:
      return state;
  }
};
