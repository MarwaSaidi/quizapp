import { publicRequest } from "../requestMethods";

export const login = async (user) => {
  try {
    const res = await publicRequest.post("/users/login", user);
    if (res.data) {
      localStorage.setItem("token", JSON.stringify(res.data.token));
      localStorage.setItem("name", JSON.stringify(res.data.name));
    }
  } catch (error) {
    return error.response.data.message;
  }
};

export const register = async (user) => {
  try {
    await publicRequest.post("/users", user);
  } catch (error) {
    return error.response.data.message;
  }
};

export const logout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("name");
};
